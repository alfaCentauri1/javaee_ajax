# JavaEE_Ajax
Ejemplo de implementación de Java EE con JSF y Ajax para las vistas.


## Contenido
[Requisitos](https://gitlab.com/alfaCentauri1/javaee_ajax/#requisitos)

*** 

## Requisitos
* JDK 11 ó superior.
* Librerías de maven.

## Visuals


## Instalación
* Compile el proyecto con el IDE de su preferencia.
* Ingrese por consola a la raíz del proyecto.
* Ejecute en la consola **java -jar target/ejemplocamel4.jar**

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

## Project status
* Developer. 